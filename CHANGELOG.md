Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.2] - 27/10/2023

- Updated `colorbutton` to version `^1.5`.
- Updated `ckeditor_font` to version `^2.0@beta`.
- Updated `panelbutton` to version `^1.5`.

## [3.0.1] - 01/11/2022
- Bump dependencies for PHP 8.1 compatibility.

## [2.0.6] - 16/08/2022

- Increase max inline image size from 5MB to 10MB.

## [2.0.5] - 25/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.0.4] - 08/02/2021

- Add core: 8.x to fix enabling issue

## [2.0.3] - 14/01/2021

- Update module to be D9-ready

## [2.0.2] - 04/12/2020

- Add composer.json file

## [2.0.1] - 07/12/2018

- Changed file structure
